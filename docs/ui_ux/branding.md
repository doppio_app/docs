# Branding

## Name

---

<span style="color:#516E5B;font-weight:800">Doppio</span> is Italian multiplier, meaning **"double"**. It is commonly called a standard double, due to its standard in judging the espresso quality in barista competitions, where four single espresso are made using two double portafilters.
[From Wikipedia](https://en.wikipedia.org/wiki/Doppio)

## Main Logo

---

### Logo

![Main](/assets/branding/Doppio.png)

### The light and Dark modes

<div style='display:flex;flex-direction:row;justify-content:space-between;flex-wrap:wrap;gap:30px'>
<img src="/assets/branding/light.png" alt='light' width='250'/>
<img src="/assets/branding/dark.png" alt='dark' width='250'/>
</div>

### Meaning

<div style='display:flex;;flex-direction:row;justify-content:space-between;flex-wrap:wrap;gap:30px'>
    <img alt='Logo Meaning' src="/assets/branding/logoMeaning.png" alt='dark' style="height:500px"/>
    <img style='align-self:center;height:350px' alt='Logo Use Case' src="/assets/branding/LogoUseCase.png"/>
</div>
<br/>
<br/>

## Secondary Logo

---

### Logo

<img alt='Secondary' src="/assets/branding/Icon.png" alt='dark' style="width:240px;height:240px"/>

### The light and Dark modes

<div style='display:flex;flex-direction:row;justify-content:space-between;flex-wrap:wrap;gap:30px'>
<img src="/assets/branding/IconFrameLight.png" alt='light' width='250'/>
<img src="/assets/branding/IconFrameDark.png" alt='dark' width='250'/>
</div>

### Meaning

<div style='display:flex;;flex-direction:row;justify-content:space-between;flex-wrap:wrap;gap:30px'>
<img alt='Icon Meaning' src="/assets/branding/IconMeaning.png" alt='dark'  style="height:500px"/>
<img style='align-self:center;height:350px' alt='Logo Meaning' src="/assets/branding/IconUseCase.png" alt='dark' />
</div>

<br/>
<br/>
