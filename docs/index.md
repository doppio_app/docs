# Overview

![Brand](/assets/branding/Doppio.png)

Doppio is a coffee app, that will allow every busy human to take a cup of loved coffee without spending time queuing.

<p align="center" style="color:#516E5B;font-weight:bold">This will make our life smarter and non-stressful.</p>
