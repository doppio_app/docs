# Authentication

Into application we have two type of authentication, Bearer and OAuth. OAuth contains authentication with Google and iCloud.

All users saved in the same `users` collection, and dedicated by `role` and `loginType`

`users` object have only essential `fields`

```yaml
{
    id : string,
    email: string,
    password: string,
    role: string,
    lastActivity: string,
    ipAddress: number,
    deviceType: string, // enum(iOS,android,web)
    loginWith: string,
    apiKey: string, //  random string
}
```

Other values which special for that user saved in the separate collection `user_profile`

```yaml
{
    id : string,
    userId: string,
    firstName: string,
    lastName: string,
    fullName: string, // generated with firstName and lastName
    createdAt: string
}
```

## Registration

For Registration user need to provide email and password, we will send some generated code to his/her provided `email`. After verification, he/she can add `firstName`, `lastName` in the next step.

Each user have unique `email` which distinguished from others. After registration `password` will be hashed with [bcrypt](https://en.wikipedia.org/wiki/Bcrypt).

After registration backend will login user and provide `access` and `refresh` tokens.

## Login

In the first we have 2 types of login

- With credentials - `email, password`
- OAuth - `google, iCloud`

The first time when user login with `Google` or `iCloud`, we will save his/her data in our database (`password` will be `null`), later he/she can add password in the settings page and also login with email and password.

Token generated with [JSON Web Token](https://jwt.io/)

## Endpoints
